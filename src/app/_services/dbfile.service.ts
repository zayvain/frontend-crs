import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from 'rxjs';
import { DBFile } from '../_models/dbfile';

@Injectable({
  providedIn: 'root'
})
export class DbfileService {

  baseUrl: string = `${environment.baseUrl}/api/reports`;
  constructor(private http: HttpClient) {}

  getAllReports(): Observable<DBFile[]> {
    return this.http.get<DBFile[]>(`${this.baseUrl}/All`);
  }

   //Get 1 program from id : /programs/{id}
   getById(id: string): Observable<DBFile> {
    return this.http.get<DBFile>(`${this.baseUrl}/${id}`);
  }

    //Delete program : /program/{id}
    deleteFile(id: string): Observable<DBFile> {
      return this.http.delete<DBFile>(`${this.baseUrl}/${id}`);
    }
  
    downloadFile(id:string):Observable<DBFile>{
      return this.http.get<DBFile>(`${this.baseUrl}/downloadFile/${id}`)
    }
}
