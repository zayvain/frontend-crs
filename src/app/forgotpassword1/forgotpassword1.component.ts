import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgotpassword1',
  templateUrl: './forgotpassword1.component.html',
  styleUrls: ['./forgotpassword1.component.css']
})
export class Forgotpassword1Component implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }
  back(){
    this.router.navigate(["/login"]);
  }
}
