import { Component, OnInit, Input } from '@angular/core';
import { Program } from 'src/app/_models/program';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-deleteprogram',
  templateUrl: './deleteprogram.component.html',
  styleUrls: ['./deleteprogram.component.css']
})
export class DeleteprogramComponent implements OnInit {
  @Input()
  program: Program;

  constructor(public activeModal: NgbActiveModal) {}

  ngOnInit() {}

  delete() {
    this.activeModal.close(this.program.id);
  }
}
